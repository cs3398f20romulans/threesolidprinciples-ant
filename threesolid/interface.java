package threesolid;

// interface segregation principle - good example
interface IWorker extends IFeedable, IWorkable {
}

interface IWorkable {
    public void work();
}

interface IFeedable{
    public void eat();
}