package threesolid;

class Worker implements IWorkable, IFeedable{
    public void work() {
    	// ....working
    }

        public void eat() {
        //.... eating in launch break
        }
}

class SuperWorker implements IWorkable, IFeedable{
    public void work() {
        //.... working much more
        System.out.println("Now Working");
    }

    public void eat() {
        //.... eating in launch break
        System.out.println("Now Eating");
        //.... eating in launch break
    }
}