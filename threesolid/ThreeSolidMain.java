package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

/*
   Open-closed principle: is followed because the classes are open to allow for extension but closed for modification.
   in other words, a class can be extended without having to change the main class.

   Single-responsibility principle: was used because we separated the classes into separate files instead of
   having a single file hold all the classes. For example, we removed the robot class, the worker class, and the
   interfaces from the main file and separated them into their own files.

   Interface segrementation principle: was used because since we separated all of the classes they are not forced to
   depend on methods that they do not use.
*/

public class ThreeSolidMain
{

    public static Manager tsManager = new Manager();

    // The entry main() method
    public static void main(String[] args)
    {

        try
        {
            System.out.format("Starting ... \n");
        }
        catch (Exception main_except)
        {
            main_except.printStackTrace();
        }

        try
        {
            System.out.format("Stopping ... \n");
        }
        catch (Exception main_except)
        {
            main_except.printStackTrace();
        }

        System.exit(0);

    }
}

